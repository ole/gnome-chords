/*  gnome-chords - a free GTK+/GNOME guitar chord finder program

    Copyright (C) 2021  Ole Aamot <ole@gnome.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _GNOME_CHORDS_CONSOLE_H_
#define _GNOME_CHORDS_CONSOLE_H_ 1

extern struct Display console;

#endif /* _GNOME_CHORDS_CONSOLE_H_ */
