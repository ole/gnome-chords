/* gnome-chords.h - a free GTK+/GNOME guitar chord finder program

   Copyright (C) 2021 Ole Aamot <ole@gnome.org>

   This program is in the public domain.
*/

#ifndef _GNOME_CHORDS_H_
#define _GNOME_CHORDS_H_

typedef  int (*_show) (char *name, int e1, int b2, int g3, int d4, int a5, int e6);
typedef  int (*_play) (char *name);
typedef void (*_fret) (int fret);
typedef void (*_text) (char *lyrics);

struct Display {
	_show show;
	_play play;
	_fret fret;
	_text text;
};

struct Chord {
	char *root;
	struct Display *ops;
	int e1;
	int b2;
	int g3;
	int d4;
	int a5;
	int e6;
};

void chord_text(char *chord, char *lyric);
void chord_loop(void);
 int chord_play(char *chord);
void chord_main(int argc, char **argv);
void chord_fail(char *chord);

#endif /* _GNOME_CHORDS_H_ */
