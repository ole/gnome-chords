/*  gnome-chords - a free GTK+/GNOME guitar chord finder program

    Copyright (C) 2021  Ole Aamot <ole@gnome.org>

    This program is in the public domain.
*/

#ifndef _GNOME_CHORDS_MAPPING_H_
#define _GNOME_CHORDS_MAPPING_H_

#include "gnome-chords-console.h"

struct Chord cm[] = {
	{"C" ,&console, 0, 1, 0, 2, 3, 0},
	{"C#",&console, 1, 2, 1, 3, 4, 0},
	{"Db",&console, 1, 2, 1, 3, 4, 0},
	{"D", &console, 1, 2, 1, 0, 0, 0},
	{"D#",&console, 3, 4, 2, 1, 0, 0},
	{"Eb",&console, 3, 4, 2, 1, 0, 0},
	{"E", &console, 0, 0, 1, 2, 2, 0},
	{"F", &console, 1, 1, 2, 3, 3, 1},
	{"F#",&console, 2, 2, 3, 4, 4, 1},
	{"Gb",&console, 2, 2, 3, 4, 4, 1},
	{"G", &console, 3, 0, 0, 0, 2, 3},
	{"G#",&console, 0, 1, 1, 1, 3, 4},
	{"Ab",&console, 0, 1, 1, 1, 3, 4},
	{"A", &console, 0, 2, 2, 2, 0, 0},
	{"A#",&console, 1, 3, 3, 3, 1, 0},
	{"Bb",&console,	1, 3, 3, 3, 1,-1},
	{"B", &console, 2, 4, 4, 4, 2, 0},
	{"Bm",&console, 2, 3, 4, 4, 2,-1},
	{NULL,NULL,     0, 0, 0, 0, 0, 0}
};

#endif /* _GNOME_CHORDS_MAPPING_H_ */
