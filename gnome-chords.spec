%define name gnome-chords
%define ver 0.0.1
%define rel 1
%define prefix %{_usr}
%define docdir %{_defaultdocdir}
Summary: Free GTK+/GNOME Guitar Chord Finder
Name: %{name}
Version: %{ver}
Release: %{rel}
License: Public Domain
Vendor: gingerblue.org
Packager: Ole Aamot
URL: https://www.gingerblue.org/src/
Group: Applications/Music
Source: https://www.gingerblue.org/src/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-root
BuildRequires: gstreamer1-devel
BuildRequires: gstreamer1-plugins-bad-free-devel
BuildRequires: gstreamer1-plugins-base-devel
Requires: gstreamer1 >= 1.8.3
Requires: gstreamer1-plugins-ugly-free >= 1.8.3
Requires: glibc
Provides: gnome-chords

%description
GNOME Chords is a Free GTK+/GNOME guitar chord finder
%prep
%setup -q

%build

%configure

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr (-,root,root)
%doc AUTHORS COPYING HACKING LICENSE NEWS README TODO
%{_bindir}/gnome-chords
%{_datadir}/gnome-chords/A.ogg
%{_datadir}/gnome-chords/B.ogg
%{_datadir}/gnome-chords/C.ogg
%{_datadir}/gnome-chords/D.ogg
%{_datadir}/gnome-chords/E.ogg
%{_datadir}/gnome-chords/F.ogg
%{_datadir}/gnome-chords/G.ogg

%changelog

* Wed Jun 16 2021  Ole Aamot
- Initial build on Fedora 34
